# Pod Syncer

This is a simple program for keeping in sync my podcast on the [AntennaPod](https://antennapod.org/)
application using the gpoddernet APIs. Documentation can be found [HERE](https://gpoddernet.readthedocs.io/en/latest/api/index.html).

**Warning!** work in progress
