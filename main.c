#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>

#define SUCCESS  0
#define ERROR   -1

#define log_location(stream) fprintf(stream, "Line %d at %s\n", __LINE__, __FILE__)

#define PORT 8080
#define MAX_MSG 64*1024
#define PARSER_BUFFER_SIZE 300


void eat_whitespaces (char **str) {
    bool should_exit = false;
    while (!should_exit) {
        switch (**str) {
          case ' ':
          case '\t':
          case '\r':
          case '\n':
            *str ++;
            break;
          default: 
            should_exit = true;
        }
    }
}

typedef enum {
    GET,
    POST,
} Request_Type;

typedef struct {
    Request_Type type;

    char **endpoint;
    int endpoint_count;

    char **field_name;
    char **field_value;
    int num_fields;

    char *content;
} Request;

typedef enum {
    DOWNLOAD, PLAY, DELETE, NEW
} Episode_Action_Type;

typedef struct {
    size_t podcast;
    char *episode;
    Episode_Action_Type type;
    char *guid;
    char *device;
    char *timestamp; // @todo maybe not a string?
    int started;
    int position;
    int total;
} Episode_Action;

void print_request (const Request request, bool print_content) {
    switch (request.type) {
      case GET:
        printf("GET ");
        break;
      case POST:
        printf("POST ");
        break;
    }
    for (int i = 0; i < request.endpoint_count; i ++) printf("/%s", request.endpoint[i]);
    printf("\n");
    for (int i = 0; i < request.num_fields; i ++) printf("%s: %s\n", request.field_name[i], request.field_value[i]);
    if (print_content) printf("%s\n-----------\n", request.content);
}

//  fills the request field with the parsed message
//  returns 0 on success and negative on error;
int parse_header (char *request_message, Request *request) {
    if (!request) {
        fprintf(stderr, "request message cannot be empty\n");
        return ERROR;
    }

    // find the method of the request
    eat_whitespaces(&request_message);
    if (strncmp(request_message, "GET", 3) == 0) {
        request->type = GET;
        request_message += 4;
    } else if (strncmp(request_message, "POST", 4) == 0) {
        request->type = POST;
        request_message += 5;
    } else {
        fprintf(stderr, "http type should be GET or POST\n");
        return ERROR;
    }

    // get the endpoint
    eat_whitespaces(&request_message);
    char *parser_buffer[PARSER_BUFFER_SIZE];
    if (*request_message == '/') request_message += 1;
    request->endpoint_count = 1;
    parser_buffer[0] = request_message;
    bool should_exit = false;
    while (!should_exit) {
        switch (*request_message) { // @todo what if we find a 0?
          case '/':
            *request_message = 0;
            request_message ++;
            parser_buffer[request->endpoint_count] = request_message;
            request->endpoint_count ++;
            break;
          case ' ':
            *request_message = 0;
            request_message ++;
            should_exit = true;
            break;
          default:
            request_message ++;
        }
    }
    request->endpoint = malloc(request->endpoint_count * sizeof(char*));
    if (request->endpoint == NULL) {
        perror("malloc");
        exit(EXIT_FAILURE);
    }
    memcpy(request->endpoint, parser_buffer, request->endpoint_count * sizeof(char*));

    // check http version
    eat_whitespaces(&request_message);
    if (memcmp(request_message, "HTTP/1.1", 8)) {
        fprintf(stderr, "http version not supported\n");
        free(request->endpoint);
        return ERROR;
    }

    // parse fields

    // @robustness what if there is not \r?
    while (*request_message != '\r') request_message ++;
    request_message ++;
    request_message ++;

    request->num_fields = 0;
    while (true) {
        if (strncmp(request_message, "\r\n", 2) == 0) {
            request_message += 2;
            break;
        }
        if (request->num_fields >= PARSER_BUFFER_SIZE/2) {
            fprintf(stderr, "We found too many fields for the parser buffer size\n");
            free(request->endpoint);
            return ERROR;
        }
        parser_buffer[request->num_fields] = request_message;
        // @robustness what if there is no ':'?
        while (*request_message != ':') request_message ++;
        *request_message = 0;
        request_message ++;
        parser_buffer[PARSER_BUFFER_SIZE/2 + request->num_fields] = request_message;
        // @robustness what if there is no \r?
        while (*request_message != '\r') request_message ++;
        *request_message = 0;
        // @robustness the skipped character should be a '\n', should we check it?
        request_message += 2;
        request->num_fields ++;
    }
 
    // move fields data from the buffer to heap
    size_t buffer_len = request->num_fields * sizeof(char*);
    request->field_name  = malloc(buffer_len);
    request->field_value = malloc(buffer_len);
    memcpy(request->field_name, parser_buffer, buffer_len);
    memcpy(request->field_value, parser_buffer + PARSER_BUFFER_SIZE/2, buffer_len);

    // fill the payload
    request->content = request_message;

    return SUCCESS;
}

int serve_auth_request (Request request, char **response) {
    if (request.endpoint_count != 5) {
        log_location(stderr);
        fprintf(stderr, "Invalid authentication format\n");
        return ERROR;
    }
    char *username = request.endpoint[3];
    if (strcmp(request.endpoint[4], "login.json") == 0) {
        // we are trying to do a login
        int i;
        for (i = 0; i < request.num_fields; i ++) 
            if (strcmp(request.field_name[i], "Authorization") == 0) break;
        if (i == request.num_fields) {
            fprintf(stderr, "No Authorization in login request\n");
            return ERROR;
        }
        char *authorization = request.field_value[i];
        while (*authorization == ' ') authorization ++;
        if (strncmp(authorization, "Basic", 5) != 0) {
            fprintf(stderr, "Expecting Basic authorization in login request\n");
            return ERROR;
        }
        while (*authorization == ' ') authorization ++;
        char *basic_auth = authorization;

        // @todo here we should check the authrozation
        bool is_auth_valid = true;
        if (is_auth_valid) {
            char *sessionid = "gallina"; // @todo what about this one??
            asprintf(response,  "HTTP/1.1 200 OK\r\nSet-Cookie: sessionid=%s\r\n\r\n", sessionid);
        } else {
            asprintf(response, "HTTP/1.1 401 Unauthorized\r\n\r\n");
        }

    } else if (strcmp(request.endpoint[4], "logout.json") == 0) {
        // we are trying to do a logout
        // @todo
    }
    return SUCCESS;
}

int serve_list_devices (Request request, char **response) {
    if (request.endpoint_count != 4) {
        log_location(stderr);
        fprintf(stderr, "Invalid authentication format\n");
        return ERROR;
    }
    char *username = request.endpoint[3];

    // request should be in the format `GET /api/2/devices/(username).json`
    char *t = username;
    for(;; t ++) {
        // @todo a username can contain a dot??
        if (*t == 0) {
            log_location(stderr);
            fprintf(stderr, "List devices should have a request terminating with .json\n");
            return ERROR;
        } else if (*t == '.') {
            *t = 0;
            break;
        }
    }

    // @todo only empty list of elements is supported
    asprintf(response, "HTTP/1.1 200 OK\r\n\r\n[]\r\n\r\n");
    return SUCCESS;
}

int serve_add_device (Request request, char **response) {
    if (request.endpoint_count != 5) {
        log_location(stderr);
        fprintf(stderr, "Invalid authentication format\n");
        return ERROR;
    }
    char *username = request.endpoint[3];

    // request should be in the format `POST /api/2/devices/(username)/(deviceid).json`
    char *device = request.endpoint[4];
    char *dot_found = 0;
    for(char *t = device; *t != 0; t ++)  if (*t == '.') dot_found = t;
    if (dot_found == 0 || strncmp(dot_found, ".json", 5) != 0) {
        log_location(stderr);
        fprintf(stderr, "List devices should have a request terminating with .json\n");
        return ERROR;
    } 
    *dot_found = 0;

    // request.content is a json containing "caption" and "type" as string field
    // possible values for type: desktop, laptop, mobile, server, other
    // @todo add these devices

    asprintf(response, "HTTP/1.1 200 OK\r\n\r\n");
    return SUCCESS;
}

int serve_get_subscriptions (Request request, char **response) {
    if (request.endpoint_count != 5) {
        log_location(stderr);
        fprintf(stderr, "bad get subscription endpoint\n");
        return ERROR;
    }

    char *username = request.endpoint[3];
    char *device   = request.endpoint[4]; // this include the .json suffix and the query

    // parse the device name
    char *dot_found = 0;
    char *t;
    for(t = device; *t != '?'; t ++) {
        if (*t == '.') dot_found = t;
        else if (*t == 0) {
            log_location(stderr);
            fprintf(stderr, "get subscription should include a since query\n");
            return ERROR;
        }
    } 
    if (dot_found == 0 || strncmp(dot_found, ".json", 5) != 0) {
        log_location(stderr);
        fprintf(stderr, "get subscriptions should have a request terminating with .json\n");
        return ERROR;
    } 
    *dot_found = 0;

    // parse the since request
    t ++;
    if (strncmp(t, "since=", 6) != 0) {
        log_location(stderr);
        fprintf(stderr, "get subscription should come with a since query\n");
        return ERROR;
    }
    t += 6;
    int since_query = atoi(t);

    // @todo returns nothing
    // @todo timestamp should be a bit more meaningful
    asprintf(response, "HTTP/1.1 200 OK\r\n\r\n{\"add\":[],\"remove\":[],\"timestamp\":0}\r\n\r\n");

    return SUCCESS;
}

// returns how many items are in the list and advance to its end
int parse_json_string_array (char **str, char ***list) {
    while (**str != '[')  (*str) ++;
    (*str) ++;

    // find how many items are in the list
    char *cursor        = *str;
    bool inside_string  = false;
    bool escaping       = false;
    bool expecting_item = true;
    int  counter        = 0;
    while (inside_string || *cursor != ']') {
        if (escaping) escaping = false;
        else if (*cursor == '\\') escaping = true;
        else if (*cursor == ',' && !inside_string) {
            expecting_item = true;
            cursor ++;
            continue;
        }

        if (expecting_item) {
            eat_whitespaces(&cursor);
            if (*cursor != '"') {
                char *start_error = cursor - 20;
                if (start_error < *str) start_error = *str;
                fprintf(stderr, "I was expecting a string here: `%s`", start_error);
            }
            expecting_item = false;
            counter ++;
        }
        cursor++;
    }

    *list = malloc(counter * sizeof(char*));

    for (int i = 0; i < counter; i ++) {
        while (**str != '"') (*str) ++;
        (*str) ++;
        char *start = *str;
        while (true) {
            if (**str == '\\') *str += 2;
            else if (**str == '"') break;
            *str += 1;
        }
        (*list)[i] = malloc(1 + *str - start);
        memcpy((*list)[i], start, *str-start);
        (*list)[i][*str-start] = 0;
        *str += 1;
    }

    *str = cursor + 1;
    return counter;
}

int serve_post_subscriptions (Request request, char **response) {
    if (request.endpoint_count != 5) {
        log_location(stderr);
        fprintf(stderr, "bad get subscription endpoint\n");
        return ERROR;
    }

    char *username = request.endpoint[3];
    char *device   = request.endpoint[4]; // this includes the .json

    // removes the .json suffix
    char *dot_found = 0;
    for(char *t = device; *t != 0; t ++)  if (*t == '.') dot_found = t;
    if (dot_found == 0 || strncmp(dot_found, ".json", 5) != 0) {
        log_location(stderr);
        fprintf(stderr, "post subscriptions should have a request terminating with .json\n");
        return ERROR;
    }
    *dot_found = 0;

    // parse the two arrays
    char **add_array = NULL;
    int add_count = 0;
    char **remove_array = NULL; 
    int remove_count = 0;

    while (*request.content != '"')  request.content ++;
    request.content ++;

    if (strncmp(request.content, "add\"", 4) == 0) {
        request.content += 4;
        add_count = parse_json_string_array(&request.content, &add_array);
    } else if (strncmp(request.content, "remove\"", 7) == 0) {
        request.content += 7;
        remove_count = parse_json_string_array(&request.content, &remove_array);
    }

    /* printf("Add:\n"); */
    /* for (int i = 0; i < add_count; i ++) printf("    %s\n", add_array[i]); */
    /* printf("\nRemove:\n"); */
    /* for (int i = 0; i < remove_count; i ++) printf("    %s\n", remove_array[i]); */

    // @todo
    asprintf(response, "HTTP/1.1 200 OK\r\n\r\n{\"timestamp\":42, \"update_urls\":[]}");
    return SUCCESS;
}

int serve_get_episodes (Request request, char **response) {
    if (request.endpoint_count != 4) {
        log_location(stderr);
        fprintf(stderr, "Wrong endpoint length in get episodes\n");
        return ERROR;
    }

    char *username = request.endpoint[3];
    char *cursor = username;
    while (*cursor != '.') cursor ++;
    *cursor = 0;
    cursor ++;

    if (strncmp(cursor, "json?since=", 11) != 0) {
        log_location(stderr);
        fprintf(stderr, "Wrong endpoint format in get episodes\n");
        return ERROR;
    }
    cursor += 11;
    int since_query = atoi(cursor);

    // @todo returns nothing
    // @todo timestamp should be a bit more meaningful
    asprintf(response, "HTTP/1.1 200 OK\r\n\r\n{\"actions\":[],\"timestamp\":0}\r\n\r\n");

    return SUCCESS;
}

int serve_post_episodes (Request request, char **response) {
    if (request.endpoint_count != 4) {
        log_location(stderr);
        fprintf(stderr, "Wrong endpoint length in post episodes\n");
        return ERROR;
    }

    char *username = request.endpoint[3];
    char *cursor = username;
    while (*cursor != '.') cursor ++;
    *cursor = 0;
    cursor ++;
    if (strncmp(cursor, "json", 5) != 0) {
        log_location(stderr);
        fprintf(stderr, "Wrong endpoint format in post episodes\n");
        return ERROR;
    }

    cursor = request.content;
    while (*cursor != '[') cursor ++;
    cursor ++;

    bool another_object = true;
    while (another_object) {
        if (*cursor != '{') {
            log_location(stderr);
            fprintf(stderr, "Wrong content format in post episodes\n");
            return ERROR;
        }
        cursor ++;

        bool should_exit = false;
        while (!should_exit) {
            eat_whitespaces(&cursor);
            if (*cursor != '"') {
                log_location(stderr);
                fprintf(stderr, "Wrong content format in post episodes\n");
                return ERROR;
            }
            cursor ++;

            char *key = cursor;
            bool is_string;
            union { char *str; int num; } value;

            bool is_escaped = false;
            while (*cursor != '"') {
                if (is_escaped) is_escaped = false;
                else if (*cursor == '\\') is_escaped = true;
                cursor ++;
            }
            *cursor = 0;
            cursor ++;
            eat_whitespaces(&cursor);
            if (*cursor != ':') {
                log_location(stderr);
                fprintf(stderr, "Wrong content format in post episodes\n");
                return ERROR;
            }
            cursor ++;
            eat_whitespaces(&cursor);
            if (*cursor == '"') {
                // we are having a string
                cursor ++;
                is_string = true;
                value.str = cursor;
                is_escaped = false;
                while (*cursor != '"') {
                    if (is_escaped) is_escaped = false;
                    else if (*cursor == '\\') is_escaped = true;
                    cursor ++;
                }
                *cursor = 0;
                cursor ++;
            } else {
                is_string = false;
                value.num = atoi(cursor);
                while (*cursor >= '0' && *cursor <= '9') cursor ++;
            }
            eat_whitespaces(&cursor);
            switch (*cursor) {
              case ',':
                break;
              case '}':
                should_exit = true;
                break;
              default:
                log_location(stderr);
                fprintf(stderr, "Wrong content format in post episodes\n");
                return ERROR;
            }
            cursor ++;
        }
        eat_whitespaces(&cursor);
        switch (*cursor) {
          case ',':
            break;
          case ']':
            another_object = false;
            break;
          default:
            log_location(stderr);
            fprintf(stderr, "Wrong content format in post episodes\n");
            return ERROR;
        }
        cursor ++;
    }

    // @todo
    asprintf(response, "HTTP/1.1 200 OK\r\n\r\n{\"timestamp\":42, \"update_urls\":[]}");
    return SUCCESS;
}

int serve_request (Request request, char **response) {
 
    // check the first elements are /api/2/
    bool is_ok = true;
    if (request.endpoint_count < 3) is_ok = false;
    if (is_ok) {
        if (strcmp(request.endpoint[0], "api") != 0) is_ok = false;
        if (strcmp(request.endpoint[1], "2")   != 0) is_ok = false;
    }
    if (!is_ok) {
        fprintf(stderr, "Request endpoint should start with /api/2/\n");
        return ERROR;
    }

    // dispatch the request to the correct endpoint
    if (strcmp(request.endpoint[2], "auth") == 0 && request.type == POST) {
        if (serve_auth_request(request, response) < 0) return ERROR;
    } else if (strcmp(request.endpoint[2], "devices") == 0 && request.type == GET) {
        if (serve_list_devices(request, response) < 0) return ERROR;
    } else if (strcmp(request.endpoint[2], "devices") == 0 && request.type == POST) {
        if (serve_add_device(request, response) < 0) return ERROR;
    } else if (strcmp(request.endpoint[2], "subscriptions") == 0 && request.type == GET) {
        if (serve_get_subscriptions(request, response) < 0) return ERROR;
    } else if (strcmp(request.endpoint[2], "subscriptions") == 0 && request.type == POST) {
        if (serve_post_subscriptions(request, response) < 0) return ERROR;
    } else if (strcmp(request.endpoint[2], "episodes") == 0 && request.type == GET) {
        if (serve_get_episodes(request, response) < 0) return ERROR;
    } else if (strcmp(request.endpoint[2], "episodes") == 0 && request.type == POST) {
        if (serve_post_episodes(request, response) < 0) return ERROR;
    }
    else {
        fprintf(stderr, "\n\nENDPOINT NOT RECOGNIZED\n");
        return ERROR;
    }

    return SUCCESS;
}

int main () {
    int server_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (server_socket == 0) {
        perror("socket");
        exit (EXIT_FAILURE);
        exit(1);
    }

    struct sockaddr_in name;
    name.sin_family = AF_INET;
    name.sin_port = htons(PORT);
    name.sin_addr.s_addr = htonl(INADDR_ANY);
    if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) < 0) {
        fprintf(stderr, "I couldn't set the reuseaddr option in server socket\n");
        perror("setsockopt");
    }
    if (bind(server_socket, (struct sockaddr *) &name, sizeof(name)) < 0) {
        perror("bind");
        exit(EXIT_FAILURE);
    }
    if (listen(server_socket, 1) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in clientname;
    socklen_t size = sizeof(clientname);
    char buffer[MAX_MSG];
    bool should_quit = false;

    while (!should_quit) {
        int connection = accept(server_socket, (struct sockaddr *) &clientname, &size);
        if (connection < 0) {
            perror("accept");
            close(server_socket);
            exit(EXIT_FAILURE);
        }

        int available_space = MAX_MSG - 1;
        char *cursor = buffer;
        char *start_payload;
        while (1) {
            int len = read(connection, cursor, available_space);
            if (len == 0) break;
            if (len < 0) {
                perror("read");
                close(connection);
                close(server_socket);
                exit(EXIT_FAILURE);
            }
            if (len >= available_space) {
                fprintf(stderr, "Msg too long\n");
                close(connection);
                close(server_socket);
                exit(EXIT_FAILURE);
            }
            *(cursor + len) = 0;
            char *start = cursor - 3;
            if (start < buffer) start = buffer;
            start_payload = strstr(start, "\r\n\r\n");
            cursor += len;
            available_space -= len;
            if (start_payload != NULL) break;
        }
        start_payload += 4;

        Request request;
        int result = parse_header(buffer, &request);
        if (result < 0) {
            fprintf(stderr, "error while parsing request (code is %d)\n", result);
            continue;
        }
        int payload_len = 0;
        for (int i = 0; i < request.num_fields; i ++) {
            if (strcmp(request.field_name[i], "Content-Length") == 0) {
                payload_len = atoi(request.field_value[i]);
            }
        }

        while (cursor - start_payload < payload_len) {
            int len = read(connection, cursor, available_space);
            if (len == 0) break;
            if (len < 0) {
                perror("read");
                close(connection);
                close(server_socket);
                exit(EXIT_FAILURE);
            }
            if (len >= available_space) {
                fprintf(stderr, "Msg too long\n");
                close(connection);
                close(server_socket);
                exit(EXIT_FAILURE);
            }
            cursor += len;
            available_space -= len;
        }
        *cursor = 0;

        char *request_message = buffer;
        print_request(request, false);

        char *response;
        if (serve_request(request, &response) == 0) {
            printf("\n >> SENDING:\n%s\n\n", response);
            write(connection, response, strlen(response));
            free(response);
        } else {
            fprintf(stderr, "error: no response sent\n");
        }

        free(request.endpoint);
        free(request.field_name);
        free(request.field_value);
        close(connection);
    }

    close(server_socket);
}
